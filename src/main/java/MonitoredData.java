import java.util.Date;

/**
 * Created by diana on 13.05.2017.
 */
public class MonitoredData {
    String activity;
    Date startDate;
    Date endDate;

    public MonitoredData(){

    }
    public MonitoredData(String activity, Date startDate, Date endDate){
        this.activity=activity;
        this.endDate=endDate;
        this.startDate=startDate;
    }
    public String getActivity(){
        return activity;
    }
    public long getTime(){
        //in minutes
        return (this.endDate.getTime()-this.startDate.getTime())/60000;
    }
    public Date getStartDate(){
        return startDate;
    }
    public String getStartDay(){
        return startDate.toString().substring(0,11);
    }
    public Date getEndDate(){
        return endDate;
    }
}
