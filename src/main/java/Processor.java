import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by diana on 14.05.2017.
 */
public class Processor {
    private List<MonitoredData> monitoredData;

    public Processor() throws IOException {
        List<String> list;
        String fileName = "activities.txt";
        Stream<String> stream = Files.lines(Paths.get(fileName));
        list = stream.collect(Collectors.toList());
        this.monitoredData = list.stream().map(temp ->
        {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String strings[] = temp.split("\t\t");
            MonitoredData myData = new MonitoredData();
            try {
                myData = new MonitoredData(strings[2].trim(), formatter.parse(strings[0]), formatter.parse(strings[1]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return myData;
        }).collect(Collectors.toList());
    }

    public static void main(String args[]) throws IOException {
        Processor processor = new Processor();
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter("file.txt");
            bw = new BufferedWriter(fw);
            bw.write("Number of days is :\n" + processor.countDays() + "\n");
            bw.write("Occurrences : \n" +
                    processor.getOccurrences() + "\n");
            bw.write("Occurrences per day : \n");
            for (Map.Entry<String, Map<String, Long>> entry : processor.getOccurrencesPerDay().entrySet())
                bw.write("\t" + entry + "\n");
            bw.write("Frequency more than 5 in 90%\n \t" + processor.getFrequency()+"\n");
            bw.write("Duration :\n \t" + processor.getDuration()+"\n");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public int countDays() {
        int number = 0;
        List<String> strings = new ArrayList<String>();
        number = (int) this.monitoredData.stream()
                .map(temp -> temp.getStartDate().toString().subSequence(0, 11))
                .distinct()
                .count();
        //jodaTime
        return number;
    }

    public Map<String, Long> getOccurrences() {
        Map<String, Long> result;
        result = this.monitoredData.stream()
                .map(temp -> temp.getActivity())
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return result;
    }

    public Map<String, Map<String, Long>> getOccurrencesPerDay() {
        Map<String, Map<String, Long>> result;
        result = this.monitoredData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getStartDay,
                        Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));

        return result;
    }

    public Map<String, Long> getDuration() {
        Map<String, Long> result1;
        result1 = this.monitoredData.stream()
                        .collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(MonitoredData::getTime)));
        Map<String, Long> result;
        result =
                result1.entrySet().stream()
                        .filter(map -> map.getValue() > 600)
                        .collect(Collectors.toMap(Map.Entry::getKey,
                                e -> (e.getValue())));

        return result;
    }

    public List<String> getFrequency() {
        Map<String, Long> list;
        list = this.monitoredData.stream()
                        .map(temp -> temp.getActivity().replaceAll("\t", ""))
                        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        Map<String, Long> list1;
        list1 = this.monitoredData.stream()
                        .filter(temp -> temp.getTime() < 5)
                        .map(temp -> temp.getActivity().replaceAll("\t", ""))
                        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        List<String> result = list1.entrySet().stream()
                .filter(entry -> entry.getValue()/list.get(entry.getKey()) > 0.9)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
        return result;
    }
}